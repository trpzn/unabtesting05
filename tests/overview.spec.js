module.exports = {
  'No muestra la lista de tareas si no se ha seteado ninguna'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .assert.elementNotPresent('.main')
      .end();
  },
  'No se muestra el footer si no se ha agregado ninguna tarea'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .assert.elementNotPresent('.footer')
      .end();
  },
  'Al cargar la pagina se setea el focus en el cuadro de texto'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .assert.elementPresent('.header .new-todo:focus')
      .end();
  },
  'Cuando agregamos una tarea, esta se muestra en la lista de tareas por hacer'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .setValue('.new-todo', 'New task')
      .keys(client.Keys.ENTER)
      .assert.containsText('.todo-list li:first-child label', 'New task')
      .end();
  },
  'Marca una tarea lista al pinchar sobre ella'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .setValue('.new-todo', 'New task')
      .keys(client.Keys.ENTER)
      .click('.todo-list li:first-child .toggle')
      .assert.cssClassPresent('.todo-list li:first-child', 'completed')
      .end();
  },
  'Muestra cuantas tareas quedan por hacer'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .setValue('.new-todo', 'New task')
      .keys(client.Keys.ENTER)
      .setValue('.new-todo', 'Another task')
      .keys(client.Keys.ENTER)
      .assert.containsText('.todo-count', '2 items left')
      .click('.todo-list li:first-child .toggle')
      .assert.containsText('.todo-count', '1 item left')
      .end();
  },
  'No se agregan las tareas vacias o en blanco'(client) {
    client
      .url('http://todomvc.com/examples/react/#/')
      .waitForElementVisible('.header h1')
      .setValue('.new-todo', 'New task')
      .keys(client.Keys.ENTER)
      .keys(client.Keys.ENTER)
      .setValue('.new-todo', '  ')
      .keys(client.Keys.ENTER)
      .assert.containsText('.todo-count', '1 item left')
      .end();
  }
}
