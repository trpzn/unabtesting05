const url = 'http://dme.unab.cl/dme/';
const user = '';
const pass = '';

module.exports = {
  'Prueba el login en '(client) {
    client
      .url(`${url}`)
      .waitForElementVisible('body')
      .click('#radiobuton_uV')
      .setValue('#username', user)
      .setValue('#label', pass)
      .click('#Submit')
      .waitForElementVisible('body')
      .assert.urlContains('pregrado2do/my')
      .end();
  }
};
